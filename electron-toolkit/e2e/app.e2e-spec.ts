import { ElectronToolkitPage } from './app.po';

describe('electron-toolkit App', () => {
  let page: ElectronToolkitPage;

  beforeEach(() => {
    page = new ElectronToolkitPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
